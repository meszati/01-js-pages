// Fix the 2 issues in the fetch
async function getRandomString(number, length, format) {
    let response = await fetch("https://www.random.org/strings/?num=1&len=4&digits=on&upperalpha=on&loweralpha=on&unique=on&format=html&rnd=new");
    return await response.json();
}

// Call getRandomString() 10 times with increasing by 1 (length++)
getRandomString(1, 16, "plain").then(data => console.log(data));
